package com.senla.intership.service.controller;

import com.senla.intership.initiator.dto.UserDto;
import com.senla.intership.service.proxy.ProxyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {
    private final ProxyService proxy;

    @GetMapping
    public ResponseEntity<UserDto> getUser() {
        return proxy.getUser();
    }
}
