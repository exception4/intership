package com.senla.intership.service.proxy;

import com.senla.intership.initiator.dto.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "user-service", url = "${application.url}")
public interface ProxyService {
    @GetMapping("/users")
    ResponseEntity<UserDto> getUser();
}

