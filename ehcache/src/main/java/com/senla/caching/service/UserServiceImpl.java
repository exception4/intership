package com.senla.caching.service;

import com.senla.caching.dao.UserRepository;
import com.senla.intership.initiator.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    @Cacheable(value = "usersCache", key = "#name")
    public UserDto getUserByFirstname(String name) {
        return userRepository.getUsers().stream()
                .filter(k -> k.getFirstName().equals(name))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("User with this name doesn't exist"));
    }
}
