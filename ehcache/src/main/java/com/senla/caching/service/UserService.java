package com.senla.caching.service;

import com.senla.intership.initiator.dto.UserDto;

public interface UserService {
    UserDto getUserByFirstname(String name);
}
