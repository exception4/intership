package com.senla.caching.dao;

import com.senla.intership.initiator.dto.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class UserRepositoryImpl implements UserRepository {

    public List<UserDto> getUsers() {
        List<UserDto> users = new ArrayList<>();
        UserDto userDto = new UserDto();
        UserDto userDto1 = new UserDto();
        userDto.setFirstName("Anton");
        userDto1.setFirstName("Artyom");
        users.add(userDto);
        users.add(userDto1);
        log.info("Retrieving list of users");
        return users;
    }
}
