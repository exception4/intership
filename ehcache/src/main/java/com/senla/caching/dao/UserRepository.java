package com.senla.caching.dao;

import com.senla.intership.initiator.dto.UserDto;

import java.util.List;

public interface UserRepository {
    List<UserDto> getUsers();
}
