package com.senla.caching.controller;

import com.senla.caching.service.UserService;
import com.senla.intership.initiator.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/{name}")
    public ResponseEntity<UserDto> getUserByFirstname(@PathVariable String name) {
        return ResponseEntity.ok(userService.getUserByFirstname(name));
    }
    @GetMapping()
    public void out() {
        List<UserDto>  users = new ArrayList<>();
        while(true){
            users.add(new UserDto());
        }
    }
}
