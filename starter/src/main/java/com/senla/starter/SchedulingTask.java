package com.senla.starter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SchedulingTask {

    @Scheduled(fixedRate = 8000)
    public void reportCurrentTime(){
        log.info("Scheduling...");
    }

}
