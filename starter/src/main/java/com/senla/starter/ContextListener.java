package com.senla.starter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ContextListener {

    @EventListener(classes = ContextRefreshedEvent.class)
    public void getMsg() {
        try {
            Thread.sleep(10000);
            log.info("My own starter...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
