package com.senla.intership.initiator.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable {
    private Long id;
    private String firstName;
    private String surname;
}
