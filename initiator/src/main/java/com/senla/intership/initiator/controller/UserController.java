package com.senla.intership.initiator.controller;

import com.senla.intership.initiator.dto.UserDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    @GetMapping
    public UserDto getUser() {
        UserDto userDto = new UserDto();
        userDto.setFirstName("Ilya");
        userDto.setId(1L);
        userDto.setSurname("Jenkins");
        return userDto;
    }
}
